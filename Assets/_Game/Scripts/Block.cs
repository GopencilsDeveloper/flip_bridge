﻿using UnityEngine;

public class Block : MonoBehaviour
{

    public Renderer rend;
    private MaterialPropertyBlock propertyBlock;

    private void Awake()
    {
        propertyBlock = new MaterialPropertyBlock();
    }

    public void SetColor(Color inputColor)
    {
        rend.GetPropertyBlock(propertyBlock);
        propertyBlock.SetColor("_Color", inputColor);
        rend.SetPropertyBlock(propertyBlock);
    }

    // private void OnTriggerEnter(Collider other)
    // {
    //     if (other.gameObject.CompareTag("BackEdge"))
    //     {
    //         Player.Instance.canRollBack = false;
    //     }

    //     if (other.gameObject.CompareTag("FowardEdge"))
    //     {
    //         Player.Instance.canRollFoward = false;
    //     }

    //     if (other.gameObject.CompareTag("RightEdge"))
    //     {
    //         Player.Instance.canRollRight = false;
    //     }

    //     if (other.gameObject.CompareTag("LeftEdge"))
    //     {
    //         Player.Instance.canRollLeft = false;
    //     }
    // }

    // private void OnTriggerExit(Collider other)
    // {
    //     if (other.gameObject.CompareTag("BackEdge"))
    //     {
    //         Player.Instance.canRollBack = true;
    //     }

    //     if (other.gameObject.CompareTag("FowardEdge"))
    //     {
    //         Player.Instance.canRollFoward = true;
    //     }

    //     if (other.gameObject.CompareTag("RightEdge"))
    //     {
    //         Player.Instance.canRollRight = true;
    //     }

    //     if (other.gameObject.CompareTag("LeftEdge"))
    //     {
    //         Player.Instance.canRollLeft = true;
    //     }
    // }
}
