﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Diamond : MonoBehaviour
{
    public float speed;
    private bool isRotating;
    public Animator aura;
    public Renderer rend;
    private Coroutine C_PingPong;

    public void Refresh()
    {
        StopRotateSelf();
        rend.enabled = true;
        aura.gameObject.SetActive(false);
    }

    public void StartRotateSelf()
    {
        isRotating = true;
    }

    public void StopRotateSelf()
    {
        isRotating = false;
    }

    private void Update()
    {
        if (isRotating)
        {
            transform.Rotate(Vector3.up * speed * Time.deltaTime);
        }
    }

    public void CollidePlayer()
    {
        C_PingPong = StartCoroutine(C_CollidePlayer());
    }

    IEnumerator C_CollidePlayer()
    {
        UIManager.Instance.AddDiamond(1);
        PlayAura();
        rend.enabled = false;
        yield return new WaitForSeconds(1f);
        DiamondPool.Instance.ReturnToPool(this);
    }

    public void PlayAura()
    {
        aura.gameObject.SetActive(true);
        aura.SetTrigger("Show");
    }

    public void DelayPingPongVert(float delay, float duration)
    {
        StartCoroutine(C_DelayPingPongVert(delay, duration));
    }

    IEnumerator C_DelayPingPongVert(float delay, float duration)
    {
        yield return new WaitForSeconds(delay);
        Vector3 lowPos = transform.position;
        Vector3 highPos = new Vector3(lowPos.x, lowPos.y + 0.5f, lowPos.z);
        while (true)
        {
            float t = 0f;

            while (t < 1f)
            {
                t += Time.deltaTime / (duration * 0.5f);
                transform.position = Vector3.Lerp(lowPos, highPos, Mathf.SmoothStep(0f, 1f, t));
                yield return null;
            }

            t = 0f;

            while (t < 1f)
            {
                t += Time.deltaTime / (duration * 0.5f);
                transform.position = Vector3.Lerp(highPos, lowPos, Mathf.SmoothStep(0f, 1f, t));
                yield return null;
            }
        }
    }
}
