﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Splash : MonoBehaviour
{
    public List<Sprite> spriteList;
    public SpriteRenderer spriteRenderer;

    public void RandomSprite()
    {
        spriteRenderer.sprite = spriteList[(int)Random.Range(0f, spriteList.Count)];
        transform.localRotation = Quaternion.Euler(90f, Random.Range(0f, 270f), 0f);
        Invoke("Reclaim", 5f);
    }

    public void SetColor(Color inputColor)
    {
        spriteRenderer.color = inputColor;
    }

    public void Reclaim()
    {
        SplashPool.Instance.ReturnToPool(this);
    }
}
