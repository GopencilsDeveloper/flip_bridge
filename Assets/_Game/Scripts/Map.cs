﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Map : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    public Player player;
    public List<Texture2D> textureList;
    public Text txtLevelTest;
    public Fever feverPrefab;

    #endregion

    #region PARAMS
    public List<Tile> tileList = new List<Tile>();
    public List<Station> stationList = new List<Station>();
    public int totalTileAmount = 0;
    public int tileUnlockedAmount = 0;
    public float unlockedPercent = 0f;
    public int index = -1;

    #endregion

    #region PROPERTIES
    public static Map Instance { get; private set; }
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        Invoke("SpawnTest", 0.1f);
    }

    public void Refresh()
    {
        ReturnAllTilesToPool();
        tileUnlockedAmount = 0;
    }

    public void ReturnAllTilesToPool()
    {
        for (int i = 0; i < tileList.Count; i++)
        {
            TilePool.Instance.ReturnToPool(tileList[i]);
        }
        tileList.Clear();
    }

    public void OnSpawnCompleted()
    {
        totalTileAmount = tileList.Count;
        tileUnlockedAmount = 0;
        unlockedPercent = 0f;

        Vector3 centerPos = tileList[(int)(tileList.Count / 2f)].transform.position;
        player.Refresh();
        player.transform.position = new Vector3(centerPos.x + 0.5f, player.currentScale.x / 2f + 0.5f, centerPos.z + 0.5f);
        player.UpdateStatus();
    }

    public void Spawn(Texture2D texture, System.Action OnSpawnCompleted)
    {
        Refresh();
        MeshInstancing.Instance.Spawn(texture);

        for (int i = 0; i < texture.width; i++)
        {
            for (int j = 0; j < texture.height; j++)
            {
                Color pixelColor = texture.GetPixel(i, j);

                if (pixelColor.a != 0f)
                {
                    Tile tile = TilePool.Instance.GetFromPool();
                    tile.transform.SetParent(PoolsLocation.Instance.tilePool);
                    tile.transform.localPosition = new Vector3(i, 0f, j);
                    tile.SetSnow(new Vector3(0f, 1.5f, 0f), new Vector3(1f, 2f, 1f), Color.white);
                    tileList.Add(tile);
                }
            }
        }

        if (OnSpawnCompleted != null)
        {
            OnSpawnCompleted();
        }
    }

    public void SpawnFeverItem()
    {
        Fever fever = Instantiate(feverPrefab);

        Vector3 randPos = tileList[(int)(Random.Range(tileList.Count / 3f, tileList.Count * (2f / 3f)))].transform.position;
        fever.transform.position = new Vector3(randPos.x, 4f, randPos.z);
        fever.StartRotateSelf();
        Debug.Log("SpawnFeverItem");
    }
    #endregion

    #region DEBUG

    string LEVELTEST = "LEVELTEST";

    void LoadLevel()
    {
        index = DataManager.Instance.currentLevel;
    }

    void SaveLevel()
    {
        PlayerPrefs.SetInt(LEVELTEST, index);
    }

    public void Next()
    {
        index++;
        index = index > textureList.Count - 1 ? 0 : index;
        Spawn(textureList[index], OnSpawnCompleted);
        txtLevelTest.text = index.ToString();
        SaveLevel();
    }

    public void Back()
    {
        index--;
        index = index < 0 ? textureList.Count - 1 : index;
        Spawn(textureList[index], OnSpawnCompleted);
        txtLevelTest.text = index.ToString();
        SaveLevel();
    }

    public void SpawnTest()
    {
        index = index < 0 ? textureList.Count - 1 : index;
        Spawn(textureList[index], OnSpawnCompleted);
        txtLevelTest.text = index.ToString();
        SaveLevel();
    }

    #endregion

}
