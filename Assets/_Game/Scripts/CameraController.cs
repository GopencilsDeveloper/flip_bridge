﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform player;
    public float smoothSpeed = 0.125f;
    public static CameraController Instance { get; private set; }
    bool isFollowing;
    public Transform target;
    Vector3 offset;


    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        offset = Camera.main.transform.position - player.position;
    }

    private void Update()
    {
        if (isFollowing)
        {
            Follow();
        }
    }

    public void Refresh()
    {
        isFollowing = true;
    }

    public void StopFollowing()
    {
        isFollowing = false;
    }

    public void Follow()
    {
        Vector3 desiredPosition = target.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        transform.position = smoothedPosition;
    }

}