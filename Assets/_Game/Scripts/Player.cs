﻿using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    public Vector3 startScale = new Vector3(8f, 2f, 2f);
    public Rigidbody rgbd;
    public float rollDurationMin;
    public float rollDurationMax;
    public float rollDurationHori;
    public float rollDurationVert;
    public Renderer rend;
    public Transform leftDetector;
    public Transform rightDetector;
    public Transform middleDetector;

    public Transform leftEdge;
    public Transform rightEdge;
    public Transform fowardEdge;
    public Transform backEdge;

    public BoxCollider leftBox;
    public BoxCollider rightBox;
    public BoxCollider backBox;
    public BoxCollider fowardBox;

    public ParticleSystem fireworkPS;
    #endregion

    #region PARAMS
    Map mapInstance;
    public Color currentColor;
    public bool isRolling;
    public bool isWin;
    public bool canRoll = true;

    public Swipe lastSwipeDirection;
    public bool isWrongMove;

    Coroutine C_Rotate;

    Vector3 startInputPos;
    Vector3 currentInputPos;
    public float triggerDistance = 10f;
    Vector3 deltaInputPos;

    public Vector3 currentScale;

    #endregion

    #region PROPERTIES
    public static Player Instance { get; private set; }
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
        mapInstance = Map.Instance;
    }

    private void OnEnable()
    {
        Refresh();
    }

    private void OnDisable()
    {
    }

    public void Refresh()
    {
        UnactiveRigidbody();
        SetStartRotation();
        SetScale(startScale);

        UpdateStatus();
        HideFireworkPS();
        UpdateUnlockedPercent();

        CameraController.Instance.Refresh();

        isWin = false;
        isRolling = false;
        isWrongMove = false;
        canRoll = true;
        isSpawnedFever = false;
    }

    private void SetStartRotation()
    {
        transform.rotation = Quaternion.Euler(0f, 0f, 90f);
    }

    public void SetScale(Vector3 scale)
    {
        currentScale = scale;
        transform.localScale = scale;
        
        leftDetector.localPosition = new Vector3(0.5f - 1.5f / currentScale.x, 0f, 0f);
        rightDetector.localPosition = new Vector3(-0.5f + 1.5f / currentScale.x, 0f, 0f);
        middleDetector.localPosition = Vector3.zero;
    }

    void HandleInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            startInputPos = currentInputPos = Input.mousePosition;
        }
        else
        if (Input.GetMouseButton(0))
        {
            currentInputPos = Input.mousePosition;
            deltaInputPos = (currentInputPos - startInputPos);

            if (deltaInputPos.magnitude < triggerDistance)
            {
                return;
            }

            if (Mathf.Abs(deltaInputPos.x) > Mathf.Abs(deltaInputPos.y))
            {
                if (Mathf.Sign(deltaInputPos.x) == 1)
                {
                    Roll(Swipe.Right);
                }
                else
                {
                    Roll(Swipe.Left);
                }
            }
            else
            {
                if (Mathf.Sign(deltaInputPos.y) == 1)
                {
                    Roll(Swipe.Up);
                }
                else
                {
                    Roll(Swipe.Down);
                }
            }
        }
        else
        if (Input.GetMouseButtonUp(0))
        {
            startInputPos = currentInputPos = Vector3.zero;
        }
    }


    private void Update()
    {
        HandleInput();
        if (Application.isEditor /* && GameManager.Instance.currentState == GameState.INGAME */)
        {
            if (isRolling)
                return;

            if (Input.GetKey(KeyCode.LeftArrow))
                Roll(Swipe.Left);

            if (Input.GetKey(KeyCode.RightArrow))
                Roll(Swipe.Right);

            if (Input.GetKey(KeyCode.UpArrow))
                Roll(Swipe.Up);

            if (Input.GetKey(KeyCode.DownArrow))
                Roll(Swipe.Down);

        }
    }

    void Roll(Swipe swipeDirection)
    {
        lastSwipeDirection = swipeDirection;

        if (isRolling || !canRoll)
            return;

        switch (swipeDirection)
        {
            case Swipe.Left:

                RotateAround(gameObject, leftEdge.position, Vector3.forward, 90f, rollDurationHori, OnRollCompleted);
                break;

            case Swipe.Right:

                RotateAround(gameObject, rightEdge.position, Vector3.back, 90f, rollDurationHori, OnRollCompleted);
                break;

            case Swipe.Up:

                RotateAround(gameObject, fowardEdge.position, Vector3.right, 90f, rollDurationVert, OnRollCompleted);
                break;

            case Swipe.Down:

                RotateAround(gameObject, backEdge.position, Vector3.left, 90f, rollDurationVert, OnRollCompleted);
                break;
        }
    }

    bool isStanding;
    [NaughtyAttributes.Button]
    public void UpdateStatus()
    {
        float offsetX = rend.bounds.extents.x;
        float offsetY = rend.bounds.extents.y;
        float offsetZ = rend.bounds.extents.z;

        //Position
        leftEdge.position = new Vector3(transform.position.x - offsetX, 0.5f, transform.position.z);
        rightEdge.position = new Vector3(transform.position.x + offsetX, 0.5f, transform.position.z);
        fowardEdge.position = new Vector3(transform.position.x, 0.5f, transform.position.z + offsetZ);
        backEdge.position = new Vector3(transform.position.x, 0.5f, transform.position.z - offsetZ);

        //Collider

        float sizeLR = Mathf.Abs(leftEdge.transform.position.z - backEdge.transform.position.z) * 2f - 0.2f;
        float sizeBF = Mathf.Abs(backBox.transform.position.x - leftBox.transform.position.x) * 2f - 0.2f;

        leftBox.size = rightBox.size = new Vector3(0.1f, 1f, sizeLR);
        backBox.size = fowardBox.size = new Vector3(sizeBF, 1f, 0.1f);

        leftBox.center = rightBox.center = backBox.center = fowardBox.center = new Vector3(0f, 0.5f, 0f);

        //Check status for updating rollDuration
        if (Mathf.Round(sizeLR) > Mathf.Round(sizeBF))  //Laying Vert
        {
            isStanding = false;
            rollDurationHori = rollDurationMin;
            rollDurationVert = rollDurationMax;
        }
        else
        if (Mathf.Round(sizeLR) < Mathf.Round(sizeBF))  //Laying Hori
        {
            isStanding = false;
            rollDurationHori = rollDurationMax;
            rollDurationVert = rollDurationMin;
        }
        else    //Standing
        {
            isStanding = true;
            rollDurationHori = rollDurationVert = rollDurationMax;
        }
    }

    void OnRollCompleted()
    {
        if (!isScalingUpFever)
        {
            CheckGrounded();
        }
        else
        {
            SetScale(new Vector3(14f, 2f, 2f));
        }
        isScalingUpFever = false;

        UpdateStatus();
        UpdateUnlockedPercent();
    }

    [NaughtyAttributes.Button]
    public void Normal()
    {
        SetScale(startScale);
        UpdateStatus();
    }

    public void RotateAround(GameObject go, Vector3 point, Vector3 axis, float angle, float inTimeSecs, System.Action OnCompleted)
    {
        C_Rotate = StartCoroutine(C_RotateAround(go, point, axis, angle, inTimeSecs, OnCompleted));
    }

    IEnumerator C_RotateAround(GameObject go, Vector3 point, Vector3 axis, float angle, float inTimeSecs, System.Action OnCompleted)
    {
        isRolling = true;

        float currentTime = 0.0f;
        float angleDelta = angle / inTimeSecs; //how many degress to rotate in one second
        float ourTimeDelta = 0;

        while (currentTime < inTimeSecs)
        {
            currentTime += Time.deltaTime;
            ourTimeDelta = Time.deltaTime;

            //Make sure we dont spin past the angle we want.
            if (currentTime > inTimeSecs)
                ourTimeDelta -= (currentTime - inTimeSecs);

            go.transform.RotateAround(point, axis, angleDelta * ourTimeDelta);
            yield return null;
        }

        isRolling = false;

        if (OnCompleted != null)
        {
            OnCompleted();
        }
    }

    [NaughtyAttributes.Button]
    void CheckGrounded()
    {
        if (!IsGrounded())
        {
            canRoll = false;
            ActiveRigidbody();
            CameraController.Instance.StopFollowing();
            // GameManager.Instance.LoseGame();
        }
        Debug.Log("CheckGrounded");
    }

    bool IsGrounded()
    {
        RaycastHit hitL, hitR;
        return Physics.SphereCast(leftDetector.position, currentScale.y / 2f - 0.2f, -Vector3.up, out hitL, 1f, 1 << 9)
        || Physics.SphereCast(rightDetector.position, currentScale.y / 2f - 0.2f, -Vector3.up, out hitR, 1f, 1 << 9)
        || Physics.SphereCast(middleDetector.position, currentScale.y / 2f - 0.2f, -Vector3.up, out hitR, 1f, 1 << 9);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(leftDetector.position - Vector3.up, currentScale.y / 2f - 0.2f);
        Gizmos.DrawWireSphere(rightDetector.position - Vector3.up, currentScale.y / 2f - 0.2f);
        Gizmos.DrawWireSphere(middleDetector.position - Vector3.up, currentScale.y / 2f - 0.2f);
    }

    void ActiveRigidbody()
    {
        rgbd.useGravity = true;
        rgbd.isKinematic = false;

        Invoke("UnactiveRigidbody", 2f);
    }

    void UnactiveRigidbody()
    {
        rgbd.useGravity = false;
        rgbd.isKinematic = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Diamond") && !isWrongMove)
        {
            other.gameObject.GetComponent<Diamond>().CollidePlayer();
            DataManager.Instance.currentDiamond++;
        }

        if (other.gameObject.CompareTag("Fever"))
        {
            Fever fever = other.gameObject.GetComponent<Fever>();
            fever.CollidePlayer();
            OnCollideFever();
        }
    }

    public bool isScalingUpFever = false;
    void OnCollideFever()
    {
        isScalingUpFever = true;
        Debug.Log("Fever");
    }

    public void ShowFireworkPS()
    {
        fireworkPS.gameObject.SetActive(true);
        fireworkPS.transform.position = new Vector3(transform.position.x, 30f, transform.position.z);
        fireworkPS.Stop();
        fireworkPS.Play();
    }

    public void HideFireworkPS()
    {
        fireworkPS.gameObject.SetActive(false);
    }

    bool isSpawnedFever;
    public void UpdateUnlockedPercent()
    {
        mapInstance.unlockedPercent = (float)mapInstance.tileUnlockedAmount / (float)mapInstance.totalTileAmount;
        UIManager.Instance.UpdatePercent(mapInstance.unlockedPercent);
        if (mapInstance.unlockedPercent >= 0.3f && !isSpawnedFever)
        {
            mapInstance.SpawnFeverItem();
            isSpawnedFever = true;
        }
    }

    #endregion

    #region DEBUG
    #endregion

}
