﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    NULL, MENU, INGAME, WINGAME, LOSEGAME
}

public class GameManager : MonoBehaviour
{
    #region Editor Params
    #endregion

    #region Params
    private static GameManager instance;
    public GameState currentState;
    bool isFirstStart;
    #endregion

    #region Properties
    public static GameManager Instance { get => instance; private set => instance = value; }
    #endregion

    #region Events
    public static event System.Action<GameState> OnStateChanged;
    #endregion

    #region Methods

    private void Awake()
    {
        instance = this;
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 300;
    }

    private void OnEnable()
    {
        RegisterEvents();
    }

    private void OnDisable()
    {
        UnregisterEvents();
    }

    private void RegisterEvents()
    {
        OnStateChanged += OnGameStateChanged;
    }

    private void UnregisterEvents()
    {
        OnStateChanged -= OnGameStateChanged;
    }

    private void Start()
    {
        Invoke("StartGame", 0.1f);
    }

    public void OnGameStateChanged(GameState state)
    {
        switch (state)
        {
            case GameState.MENU:
                break;
            case GameState.INGAME:
                break;
            case GameState.WINGAME:
                DataManager.Instance.currentLevel++;
                DataManager.Instance.SaveData();
                break;
            case GameState.LOSEGAME:
                DataManager.Instance.SaveData();
                break;
        }
    }

    public void StartGame()
    {
        ChangeState(GameState.MENU);
        Map.Instance.Spawn(Map.Instance.textureList[DataManager.Instance.currentLevel], Map.Instance.OnSpawnCompleted);
        isFirstStart = true;
    }

    public void PlayGame()
    {
        ChangeState(GameState.INGAME);
        if (isFirstStart)
        {
            isFirstStart = false;
            return;
        }
        Map.Instance.Spawn(Map.Instance.textureList[DataManager.Instance.currentLevel], Map.Instance.OnSpawnCompleted);
    }

    public void RestartGame()
    {
        ChangeState(GameState.INGAME);
    }

    public void WinGame()
    {
        ChangeState(GameState.WINGAME);
    }

    public void LoseGame()
    {
        ChangeState(GameState.LOSEGAME);
    }

    public void ChangeState(GameState state)
    {
        currentState = state;
        if (OnStateChanged != null)
        {
            OnStateChanged(state);
        }
    }

    #endregion
}
