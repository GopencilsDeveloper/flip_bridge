﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIManager : MonoBehaviour
{
    #region Editor Params
    [Header("SCREENS")]
    public GameObject Menu;
    public GameObject InGame;
    public GameObject WinGame;
    public GameObject LoseGame;

    [Header("INGAME")]
    public Text txtScore;
    public Animator txtScoreAnimator;
    public Text txtLevelLeft;
    public Text txtLevelRight;
    public Image imgFill;

    [Header("WINGAME")]
    public Image imgStar1;
    public Animator imgStar1Animator;
    public Image imgStar2;
    public Animator imgStar2Animator;
    public Image imgStar3;
    public Animator imgStar3Animator;

    #endregion

    #region Params
    int totalDiamond = 10;
    public int currentDiamond;
    int currentScore;
    #endregion

    #region Properties
    public static UIManager Instance { get; private set; }
    #endregion

    #region Events
    #endregion

    #region Methods

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        RegisterEvents();
    }

    private void OnDisable()
    {
        UnregisterEvents();
    }

    private void RegisterEvents()
    {
        GameManager.OnStateChanged += OnGameStateChanged;
    }

    private void UnregisterEvents()
    {
        GameManager.OnStateChanged -= OnGameStateChanged;
    }

    public void OnGameStateChanged(GameState state)
    {
        switch (state)
        {
            case GameState.MENU:
                UpdateLevel();
                OnMenu();
                break;
            case GameState.INGAME:
                currentDiamond = 0;
                UpdateLevel();
                OnInGame();
                break;
            case GameState.WINGAME:
                OnWinGame();
                break;
            case GameState.LOSEGAME:
                OnLoseGame();
                break;
        }
    }

    public void OnMenu()
    {
        Menu.SetActive(true);
        InGame.SetActive(true);
        WinGame.SetActive(false);
        LoseGame.SetActive(false);
    }

    public void OnInGame()
    {
        Menu.SetActive(false);
        InGame.SetActive(true);
        WinGame.SetActive(false);
        LoseGame.SetActive(false);
    }

    public void OnWinGame()
    {
        Debug.Log("Win!");
        Menu.SetActive(false);
        WinGame.SetActive(true);
        LoseGame.SetActive(false);
        // StartCoroutine(C_ShowWinGame());
    }

    // IEnumerator C_ShowWinGame()
    // {
    //     float percent = (float)currentDiamond / (float)Map.Instance.totalDiamondInMap;
    //     imgStar1.gameObject.SetActive(false);
    //     imgStar2.gameObject.SetActive(false);
    //     imgStar3.gameObject.SetActive(false);

    //     if ((percent >= 0.24f) && (percent <= 0.4f))
    //     {
    //         imgStar1.gameObject.SetActive(true);
    //         imgStar1Animator.SetTrigger("Bubble");
    //     }
    //     else
    //     if ((percent >= 0.6f) && (percent <= 0.85f))
    //     {
    //         imgStar1.gameObject.SetActive(true);
    //         imgStar1Animator.SetTrigger("Bubble");
    //         yield return new WaitForSeconds(0.5f);

    //         imgStar2.gameObject.SetActive(true);
    //         imgStar2Animator.SetTrigger("Bubble");
    //     }
    //     else
    //     if ((percent >= 0.95f))
    //     {
    //         imgStar1.gameObject.SetActive(true);
    //         imgStar1Animator.SetTrigger("Bubble");
    //         yield return new WaitForSeconds(0.5f);

    //         imgStar2.gameObject.SetActive(true);
    //         imgStar2Animator.SetTrigger("Bubble");
    //         yield return new WaitForSeconds(0.5f);

    //         imgStar3.gameObject.SetActive(true);
    //         imgStar3Animator.SetTrigger("Bubble");
    //     }
    //     else
    //     {
    //         imgStar1.gameObject.SetActive(false);
    //         imgStar2.gameObject.SetActive(false);
    //         imgStar3.gameObject.SetActive(false);
    //     }
    // }

    public void OnLoseGame()
    {
        Debug.Log("Lose!");
        Menu.SetActive(false);
        WinGame.SetActive(false);
        LoseGame.SetActive(true);
    }

    public void AddDiamond(int count)
    {
        currentDiamond += count;
    }

    // IEnumerator C_UpdateDiamond()
    // {
    //     float start = imgStarsFill.fillAmount;
    //     float end = (float)currentDiamond / (float)Map.Instance.totalDiamondInMap;

    //     if ((end >= 0.24f && end <= 0.35f)
    //         || (end >= 0.63f && end <= 0.725f)
    //         || (end >= 0.95f))
    //     {
    //         imgStarsFillAnimator.SetTrigger("Bubble");
    //     }

    //     float t = 0f;
    //     while (t < 1f)
    //     {
    //         t += Time.deltaTime / 0.5f;
    //         imgStarsFill.fillAmount = Mathf.Lerp(start, end, Mathf.SmoothStep(0f, 1f, t));
    //         yield return null;
    //     }
    // }

    public void AddScore(int addedScore)
    {
        currentScore += addedScore;
        UpdateScore();
    }

    public void UpdateScore()
    {
        txtScore.text = currentDiamond.ToString();
        txtScore.DOText(currentScore.ToString(), 0.5f, true, ScrambleMode.Numerals, null);
        txtScoreAnimator.SetTrigger("Bubble");
    }

    public void UpdateLevel()
    {
        txtLevelLeft.text = (DataManager.Instance.currentLevel + 1).ToString();
        txtLevelRight.text = (DataManager.Instance.currentLevel + 2).ToString();
    }

    public void UpdatePercent(float percent)
    {
        imgFill.fillAmount = percent;
    }

    #endregion

    #region DEBUG
    #endregion
}