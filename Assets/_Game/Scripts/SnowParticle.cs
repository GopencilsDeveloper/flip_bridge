﻿using UnityEngine;

public class SnowParticle : MonoBehaviour
{
    public ParticleSystem particleSystem;

    private void OnEnable()
    {
        Invoke("Reclaim", 1.5f);
    }

    public void Reclaim()
    {
        SnowParticlePool.Instance.ReturnToPool(this);
    }

    public void SetColor(Color inputColor)
    {
        var main = particleSystem.main;
        main.startColor = inputColor;
    }
}
