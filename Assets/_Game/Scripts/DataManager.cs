﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    #region CONST
    public const string LEVEL = "LEVEL";
    public const string SCORE = "SCORE";
    public const string DIAMOND = "DIAMOND";
    #endregion

    #region Editor Params
    #endregion

    #region Params
    public int currentLevel;
    public int currentScore;
    public int totalScore;
    public int currentDiamond;
    public int totalDiamond;

    #endregion

    #region Properties
    public static DataManager Instance { get; private set; }
    #endregion

    #region Events
    #endregion

    #region Methods

    private void Awake()
    {
        Instance = this;
        LoadData();
        // ResetData();
        // PlayerPrefs.SetInt(LEVEL, 50);
        // currentLevel = PlayerPrefs.GetInt(LEVEL);
    }

    public void LoadData()
    {
        currentLevel = PlayerPrefs.GetInt(LEVEL);
        totalScore = PlayerPrefs.GetInt(SCORE);
        currentScore = 0;
        totalDiamond = PlayerPrefs.GetInt(DIAMOND);
        currentDiamond = 0;
    }

    public void SaveData()
    {
        PlayerPrefs.SetInt(LEVEL, currentLevel);
        totalScore += currentScore;
        PlayerPrefs.SetInt(SCORE, totalScore);

        totalDiamond += currentDiamond;
        PlayerPrefs.SetInt(DIAMOND, totalDiamond);
    }

    public void ResetData()
    {
        currentScore = 0;
        currentDiamond = 0;
        PlayerPrefs.SetInt(LEVEL, 0);
        PlayerPrefs.SetInt(SCORE, 0);
        PlayerPrefs.SetInt(DIAMOND, 0);
        SaveData();
    }
    #endregion
}
