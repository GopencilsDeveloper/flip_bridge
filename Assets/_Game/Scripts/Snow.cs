﻿using UnityEngine;

public class Snow : MonoBehaviour
{
    public Renderer rend;
    private MaterialPropertyBlock propBlock;

    private void Awake()
    {
        propBlock = new MaterialPropertyBlock();
    }

    public void SetColor(Color inputColor)
    {
        rend.GetPropertyBlock(propBlock);
        propBlock.SetColor("_Color", inputColor);
        rend.SetPropertyBlock(propBlock);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            SnowParticle snowPS = SnowParticlePool.Instance.GetFromPool();
            snowPS.transform.position = this.transform.position;
            snowPS.transform.SetParent(PoolsLocation.Instance.snowParticlePool);

            DataManager.Instance.currentScore++;
            UIManager.Instance.AddScore(1);
            Map.Instance.tileUnlockedAmount++;

            gameObject.SetActive(false);
        }
    }
}
