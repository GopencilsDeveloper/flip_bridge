﻿using UnityEngine;

public class Destination : MonoBehaviour
{
    public Renderer rend;
    private MaterialPropertyBlock propertyBlock;

    private void Awake()
    {
        propertyBlock = new MaterialPropertyBlock();
    }

    public void SetColor(Color inputColor)
    {
        rend.GetPropertyBlock(propertyBlock);
        propertyBlock.SetColor("_Color", inputColor);
        rend.SetPropertyBlock(propertyBlock);
    }
}
