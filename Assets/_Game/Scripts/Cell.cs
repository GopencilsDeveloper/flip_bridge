﻿using UnityEngine;

public class Cell : MonoBehaviour
{
    public Color currentColor;
    public void Initialize(Color color)
    {
        this.currentColor = color;
    }
}
