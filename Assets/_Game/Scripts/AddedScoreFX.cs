﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddedScoreFX : MonoBehaviour
{
    public void DelayReclaim(float delay = 0f)
    {
        StartCoroutine(C_DelayReclaim(delay));
    }

    IEnumerator C_DelayReclaim(float delay)
    {
        yield return new WaitForSeconds(delay);
        AddedScoreFXPool.Instance.ReturnToPool(this);
    }
}
