﻿using UnityEngine;

public class PoolsLocation : MonoBehaviour
{
    public Transform tilePool;
    public Transform diamondPool;
    public Transform destinationPool;
    public Transform blockPool;
    public Transform addedScoreFXPool;
    public Transform snowParticlePool;

    public static PoolsLocation Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }
}
