﻿using UnityEngine;

public class Tile : MonoBehaviour
{
    public Material coloredMat;
    public Snow snow;
    private MaterialPropertyBlock propertyBlock;
    bool isCollidedPlayer;

    private void Awake()
    {
        propertyBlock = new MaterialPropertyBlock();
    }

    private void OnEnable()
    {
        snow.gameObject.SetActive(false);
        isCollidedPlayer = false;
    }

    public void SetSnow(Vector3 lPos, Vector3 lScale, Color inputColor)
    {
        snow.gameObject.SetActive(true);
        snow.transform.localPosition = lPos;
        snow.transform.localScale = lScale;
        snow.SetColor(inputColor);
    }

    // private void OnTriggerEnter(Collider other)
    // {
    //     if (other.gameObject.CompareTag("Player") && !isCollidedPlayer)
    //     {
    //         isCollidedPlayer = true;

    //         DataManager.Instance.currentScore++;
    //         UIManager.Instance.AddScore(1);
    //     }
    // }
}
