﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Station : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    public BoxCollider box;
    #endregion

    #region PARAMS
    public List<Tile> tileStationList = new List<Tile>();
    bool isWaved = false;
    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS


    public void Spring(GameObject go, float duration, float delay)
    {
        StartCoroutine(C_Spring(go, duration, delay));
    }

    IEnumerator C_Spring(GameObject go, float duration, float delay)
    {
        Vector3 startPos = go.transform.position;
        Vector3 endPos = new Vector3(startPos.x, startPos.y * 0.9f, startPos.z);

        yield return new WaitForSeconds(delay);

        float t = 0f;
        while (t < 1f)
        {
            t += Time.deltaTime / duration * 2f;

            go.transform.position = Vector3.Lerp(startPos, endPos, Mathf.SmoothStep(0f, 1f, t));

            yield return null;
        }

        t = 0f;

        while (t < 1f)
        {
            t += Time.deltaTime / duration * 2f;

            go.transform.position = Vector3.Lerp(endPos, startPos, Mathf.SmoothStep(0f, 1f, t));

            yield return null;
        }
    }

    public void Refresh()
    {
        tileStationList.Clear();
        isWaved = false;
    }

    public void AddTileToList(Tile tile)
    {
        tileStationList.Add(tile);
    }

    public void SetCollider()
    {
        if (tileStationList.Count > 0)
        {
            float sizeX =
            Mathf.Abs(tileStationList[tileStationList.Count - 1].transform.position.x - tileStationList[0].transform.position.x) + 1f;

            float sizeZ =
            Mathf.Abs(tileStationList[tileStationList.Count - 1].transform.position.z - tileStationList[0].transform.position.z) + 1f;

            box.size = new Vector3(sizeX, 1f, sizeZ);
            box.center =
             new Vector3(
                sizeX * 0.5f + tileStationList[0].transform.position.x - 0.5f - transform.position.x,
                0f,
                sizeZ * 0.5f + tileStationList[0].transform.position.z - 0.5f - transform.position.z);
        }
    }

    public void Wave(float duration)
    {
        int count = tileStationList.Count;
        float delayStep = (float)duration / (float)count;
        if (count > 0)
        {
            for (int i = 0; i < count; i++)
            {
                Tile tile = tileStationList[i];
                Spring(tile.gameObject, 0.2f, i * delayStep);
            }
            isWaved = true;
        }
        else
        {
            Debug.Log("This Station is empty!", gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // if (other.gameObject.CompareTag("Player") && !isWaved)
        // {
        //     Wave(1.5f);
        // }
    }

    #endregion

    #region DEBUG
    #endregion

}
