﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fever : MonoBehaviour
{
    private bool isRotating;
    public float speed;

    public void StartRotateSelf()
    {
        isRotating = true;
    }

    public void StopRotateSelf()
    {
        isRotating = false;
    }

    private void Update()
    {
        if (isRotating)
        {
            transform.Rotate(Vector3.up * speed * Time.deltaTime);
        }
    }

    public void CollidePlayer()
    {
        StartCoroutine(C_CollidePlayer());
    }

    IEnumerator C_CollidePlayer()
    {
        Debug.Log("CollidePlayer");
        UIManager.Instance.AddDiamond(1);
        PlayAura();
        yield return null;
    }

    public void PlayAura()
    {
    }
}
